#!/usr/bin/env python3

from libs import data_access, operations, ui

labels = data_access.get_lines_from_file("data/chars.txt")
action = operations.copy_to_clipboard
GUI = ui.build_ui(labels, action)

GUI.mainloop()
