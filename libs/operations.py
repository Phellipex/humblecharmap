def copy_to_clipboard(data_to_copy, tkinter_window):
    """Takes the data_to_copy and copies it to the clipboard using tkinter"""
    tkinter_window.clipboard_clear()
    tkinter_window.clipboard_append(data_to_copy)
