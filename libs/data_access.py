def get_lines_from_file(path_to_file):
    """Opens the file at path_to_file and returns its lines as a list of str"""

    with open(path_to_file, "r") as data_file:
        data = [a_line.strip() for a_line in data_file.readlines()]
    return data
