import tkinter


def build_ui(list_of_button_labels, button_action):
    """Returns a GUI object with as many butttons as characters
    in the list_of_button_labels. Each button will be associated
    with the action passed in button_action"""

    window = tkinter.Tk()
    frame = tkinter.Frame(window).pack()

    window.title("CHARS")
    window.wm_attributes("-topmost", 1)
    window.resizable(0, 0)

    for character in list_of_button_labels:
        tkinter.Button(frame, text=character, command=lambda current_char=character:
        button_action(current_char, window)).pack(side="left")

    tkinter.Button(frame, text="Salir", width=8, command=window.destroy).pack(side="right", padx=10)

    return window
